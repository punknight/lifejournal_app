class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def hello
    render html: '<ul><li>Starcraft</li><li>Chess</li><li>WorkingOut</li><li>EatingHealthy</li></ul>'.html_safe
  end
end