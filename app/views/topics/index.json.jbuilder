json.array!(@topics) do |topic|
  json.extract! topic, :id, :interest, :website
  json.url topic_url(topic, format: :json)
end
