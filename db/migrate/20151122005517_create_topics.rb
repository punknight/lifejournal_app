class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string :interest
      t.string :website

      t.timestamps null: false
    end
  end
end
